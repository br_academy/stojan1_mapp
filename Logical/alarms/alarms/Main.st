
PROGRAM _INIT
		 
END_PROGRAM

PROGRAM _CYCLIC

	MpAlarmXCore_0(MpLink := ADR(gAlarmXCore) , Enable := TRUE);
	
	MpAlarmXAcknowledgeAll_0(MpLink := ADR(gAlarmXCore), Enable := TRUE);
	 
END_PROGRAM

PROGRAM _EXIT
	
	MpAlarmXCore_0(Enable := FALSE);
	 
END_PROGRAM

