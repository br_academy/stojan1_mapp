
PROGRAM _INIT
	
	axis2par.Velocity := 10;
	axis2par.Acceleration := 10;
	axis2par.Deceleration := 10;
	
	axis2.MpLink := ADR(axis2ref);
	axis2.Parameters := ADR(axis2par);
	axis2.Enable := TRUE;
	
	State := 0;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	CASE State OF
		0:
			// Wait
		
		1: 
			// Power on
			IF (axis2.Info.ReadyToPowerOn) THEN
				axis2.Power := TRUE;
				State := 2;
			END_IF
		
		2:
			// Home
			IF (axis2.PowerOn) THEN
				axis2.Home := TRUE;
				State := 3;
			ELSIF (axis2.Error) THEN
				axis2.Power := FALSE;
				State := 111;
			END_IF;
				
		3:
			// Finish homing
			IF (axis2.IsHomed) THEN
				axis2.Home := FALSE;
				State := 4;
			ELSIF (axis2.Error) THEN
				axis2.Power := FALSE;
				axis2.Home := FALSE;
				State := 111;
			END_IF;
			
		4:
			// Powered, homed, ready for movement
			
			
		5:
			// Start test movement - revolution forward and backward
			IF (axis2.PowerOn AND axis2.IsHomed) THEN
				axis2par.Position := 1;
				axis2.MoveAbsolute := TRUE;
				State := 6;
			END_IF
			
		6:
			IF (axis2.MoveDone) THEN
				axis2.MoveAbsolute := FALSE;
				State := 7;
			ELSIF (axis2.Error) THEN
				State := 111;
			END_IF
			
		7:
			axis2par.Position := 0;
			axis2.MoveAbsolute := TRUE;
			State := 8;
			
		8:
			IF (axis2.MoveDone) THEN
				axis2.MoveAbsolute := FALSE;
				State := 4;
			ELSIF (axis2.Error) THEN
				State := 111;
			END_IF
			
		111:
		// Error
		
	END_CASE
	
	axis2();
	 
END_PROGRAM



PROGRAM _EXIT
	
	axis2.Enable := FALSE;
	axis2();
	 
END_PROGRAM