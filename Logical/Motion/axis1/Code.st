
PROGRAM _INIT

	axis1par.Velocity := 10;
	axis1par.Acceleration := 10;
	axis1par.Deceleration := 10;
		
	axis1.MpLink := ADR(axis1ref);
	axis1.Parameters := ADR(axis1par);
	axis1.Enable := TRUE;
	
	State := 0;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	CASE State OF
		0:
			// Wait
		
		1: 
			// Power on
			IF (axis1.Info.ReadyToPowerOn) THEN
				axis1.Power := TRUE;
				State := 2;
			END_IF
		
		2:
			// Home
			IF (axis1.PowerOn) THEN
				axis1.Home := TRUE;
				State := 3;
			ELSIF (axis1.Error) THEN
				axis1.Power := FALSE;
				State := 111;
			END_IF;
				
		3:
			// Finish homing
			IF (axis1.IsHomed) THEN
				axis1.Home := FALSE;
				State := 4;
			ELSIF (axis1.Error) THEN
				axis1.Power := FALSE;
				axis1.Home := FALSE;
				State := 111;
			END_IF;
			
		4:
			// Powered, homed, ready for movement
			
			
		5:
			// Start test movement - revolution forward and backward
			IF (axis1.PowerOn AND axis1.IsHomed) THEN
				axis1par.Position := 1;
				axis1.MoveAbsolute := TRUE;
				State := 6;
			END_IF
			
		6:
			IF (axis1.MoveDone) THEN
				axis1.MoveAbsolute := FALSE;
				State := 7;
			ELSIF (axis1.Error) THEN
				State := 111;
			END_IF
			
		7:
			axis1par.Position := 0;
			axis1.MoveAbsolute := TRUE;
			State := 8;
			
		8:
			IF (axis1.MoveDone) THEN
				axis1.MoveAbsolute := FALSE;
				State := 4;
			ELSIF (axis1.Error) THEN
				State := 111;
			END_IF
			
		111:
			// Error
		
	END_CASE
	
	axis1();
	 
END_PROGRAM



PROGRAM _EXIT
	
	axis1.Enable := FALSE;
	axis1();
	 
END_PROGRAM